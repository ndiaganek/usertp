package com.example.ndiaganek.usertp.mvvm.model;

import com.example.ndiaganek.usertp.data.EmployeDao;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EmployeModelImpl implements EmployeModel {

    private final EmployeDao employeDao;

    public EmployeModelImpl(EmployeDao employeDao) {
        this.employeDao = employeDao;
    }

    @Override
    public void getListEmploye(final GetListCallBack callBack) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                List<Employe> employe = employeDao.getEmploye();
                callBack.onDataLoaded(employe);
            }
        });
    }

    @Override
    public void addEmploye(Employe employe) {
        employeDao.addEmploye(employe);
    }
}
