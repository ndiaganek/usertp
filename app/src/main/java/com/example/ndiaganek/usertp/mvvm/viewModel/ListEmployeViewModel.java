package com.example.ndiaganek.usertp.mvvm.viewModel;

import com.example.ndiaganek.usertp.mvvm.model.Employe;
import com.example.ndiaganek.usertp.mvvm.model.EmployeModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

public class ListEmployeViewModel extends ViewModel {

    // Retient la liste de employes renvoyée par le mode
    private MutableLiveData<List<Employe>> liveData = new MutableLiveData<>();

    // Model qui permet de recuperer la liste des employes depuis
    // la base de donnees Room
    private final EmployeModel employeModel;

    public ListEmployeViewModel(EmployeModel employeModel) {
        this.employeModel = employeModel;
    }

    // getter de livedata. Methode utilisée par la View pour
    // Observer la liste des employes
    public LiveData<List<Employe>> getEmployeLiveData() {
        return liveData;
    }

    // Permet à la vue de demander au ViewModel la liste des employés
    public void getEmploye() {
        employeModel.getListEmploye(new EmployeModel.GetListCallBack() {
            @Override
            public void onDataLoaded(List<Employe> employes) {
                liveData.postValue(employes);
            }
        });
    }
}
