package com.example.ndiaganek.usertp.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ndiaganek.usertp.R;
import com.example.ndiaganek.usertp.mvvm.model.Employe;

import java.util.List;
import java.util.zip.Inflater;

class EmployeAdapter extends RecyclerView.Adapter<EmployeAdapter.ViewHolder>{

    private List<Employe> listEmployes;

    public EmployeAdapter(List<Employe> listEmployes) {
        this.listEmployes = listEmployes;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        View view = from.inflate(R.layout.item_list_employe, viewGroup);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Employe employe = listEmployes.get(i);
        viewHolder.txtNom.setText(employe.getPrenom()+" "+employe.getNom());
        viewHolder.txtFonction.setText(employe.getFontion());
    }

    @Override
    public int getItemCount() {
        return listEmployes.size();
    }

    public void update(List<Employe> employes) {
        listEmployes.clear();
        listEmployes.addAll(employes);
        notifyDataSetChanged();
    }

     class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtNom;
        public TextView txtFonction;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNom=itemView.findViewById(R.id.id_nom_employe);
            txtFonction=itemView.findViewById(R.id.id_fonction_employe);

        }
    }
}
