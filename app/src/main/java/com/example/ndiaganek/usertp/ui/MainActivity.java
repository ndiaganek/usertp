package com.example.ndiaganek.usertp.ui;

import com.example.ndiaganek.usertp.App;
import com.example.ndiaganek.usertp.R;
import com.example.ndiaganek.usertp.mvvm.model.Employe;
import com.example.ndiaganek.usertp.mvvm.model.EmployeModel;
import com.example.ndiaganek.usertp.mvvm.model.EmployeModelImpl;
import com.example.ndiaganek.usertp.mvvm.viewModel.ListEmployeViewModel;
import com.example.ndiaganek.usertp.mvvm.viewModel.ListEmployeViewModelFactory;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EmployeAdapter employeAdapter;

    private ListEmployeViewModel viewModel;

    private RecyclerView recyclerView;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView=findViewById(R.id.id_recyclerview_employe);
        textView=findViewById(R.id.id_textView_empty);

        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        employeAdapter=new EmployeAdapter(new ArrayList<Employe>());

        recyclerView.setAdapter(employeAdapter);

        EmployeModel model = new EmployeModelImpl(App.employeDatabase.getEmployeDao());


        ListEmployeViewModelFactory factory = new ListEmployeViewModelFactory(model);
        viewModel = ViewModelProviders.of(this, factory).get(ListEmployeViewModel.class);

        viewModel.getEmployeLiveData().observe(this, new Observer<List<Employe>>() {
            @Override
            public void onChanged(@Nullable List<Employe> employes) {
                if (employes == null) {
                    showEmpty();
                }
                else if (employes.isEmpty()){
                    showEmpty();
                }
                else{
                    showEmployes(employes);
                }
            }
        });
        viewModel.getEmploye();
    }

    private void showEmployes(List<Employe> employes) {
        textView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        employeAdapter.update(employes);
    }

    private void showEmpty() {
        textView.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_add) {
            Intent intent = new Intent(this, AddEmployeActivity.class);
            startActivity(intent);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
