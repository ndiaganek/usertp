package com.example.ndiaganek.usertp.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.ndiaganek.usertp.mvvm.model.Employe;

import java.util.List;

@Dao
public interface EmployeDao {

    @Query("SELECT * FROM employe")
    List<Employe> getEmploye();

    @Insert
    void addEmploye(Employe employe);
}
