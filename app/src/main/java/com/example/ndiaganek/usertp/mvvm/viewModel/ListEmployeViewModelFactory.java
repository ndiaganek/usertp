package com.example.ndiaganek.usertp.mvvm.viewModel;

import com.example.ndiaganek.usertp.mvvm.model.EmployeModel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

/**
 * Created on 8/11/18 at 10:48 AM
 * Project name : usertp
 */
public class ListEmployeViewModelFactory implements ViewModelProvider.Factory {

    private EmployeModel employeModel;

    public ListEmployeViewModelFactory(EmployeModel employeModel) {
        this.employeModel = employeModel;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ListEmployeViewModel(employeModel);
    }
}
