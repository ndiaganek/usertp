package com.example.ndiaganek.usertp.mvvm.model;

import java.util.List;

public interface EmployeModel {

     void getListEmploye(GetListCallBack callBack);

     void addEmploye(Employe employe);

     interface GetListCallBack {
          void onDataLoaded(List<Employe> employes);
     }

}
