package com.example.ndiaganek.usertp.mvvm.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Employe {

    @PrimaryKey
    private int id;
    private String nom;
    private String prenom;
    private String fontion;
    private int age;

    public Employe(int id,String nom, String prenom, String fontion, int age) {
        this.id=id;
        this.nom = nom;
        this.prenom = prenom;
        this.fontion = fontion;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getFontion() {
        return fontion;
    }

    public void setFontion(String fontion) {
        this.fontion = fontion;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Employe{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", fontion='" + fontion + '\'' +
                ", age=" + age +
                '}';
    }
}
