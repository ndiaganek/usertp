package com.example.ndiaganek.usertp.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.ndiaganek.usertp.mvvm.model.Employe;

@Database(entities = {Employe.class}, version = 1)
public abstract class EmployeDatabase extends RoomDatabase{


    public abstract EmployeDao getEmployeDao();
}
