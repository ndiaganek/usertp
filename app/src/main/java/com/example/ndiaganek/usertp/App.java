package com.example.ndiaganek.usertp;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import com.example.ndiaganek.usertp.data.EmployeDatabase;

public class App extends Application{

    public static EmployeDatabase employeDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        employeDatabase = Room.databaseBuilder(this, EmployeDatabase.class, "employe_db").build();
    }
}
